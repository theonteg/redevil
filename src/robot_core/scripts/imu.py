#!/usr/bin/env python
import rospy, math, tf, serial, std_msgs.msg
from sensor_msgs.msg import Imu
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus

def imu_publish():
	rospy.init_node('imu_node', anonymous=True)
	imu_port = rospy.get_param('~port', '/dev/ttyUSB0')
	imu_baud = rospy.get_param('~baud', 115200)
	imu_rate = rospy.get_param('~rate', 20)
	imu_frame = rospy.get_param('~frame_id', 'imu')
	pub = rospy.Publisher('imu', Imu, queue_size=2)
	#for posting magnetometer orientation in seperate topic
	pub2 = rospy.Publisher('imu_mag', Imu, queue_size=2)
	diagn = rospy.Publisher('diagnostics', DiagnosticArray, queue_size=2)
	rate = rospy.Rate(imu_rate)
	
	diag_msg = DiagnosticArray()

	maxmagx = - float("inf")
	minmagx = float("inf")
	maxmagy = - float("inf")
	minmagy = float("inf")
	maxmagz = - float("inf")
	minmagz = float("inf")
	heading = 0

	while not rospy.is_shutdown():
		try:
			ser = serial.Serial(port = imu_port, baudrate = imu_baud)
			
			#Publish diagnostics status ok
			diag_msg.status = [
				DiagnosticStatus(name = 'IMU', level = DiagnosticStatus.OK, message = 'OK', hardware_id = 'IMU')
			]
			diag_msg.header.stamp = rospy.Time.now()
			diagn.publish(diag_msg)
			rospy.loginfo("Connected to IMU")
			msg = Imu()
			msg.header.frame_id = imu_frame
			
			msg_mag = Imu()
			msg_mag.header.frame_id = imu_frame + "_mag"

			#Publish IMU readings
			while not rospy.is_shutdown():
				msg.header.stamp = rospy.Time.now()
				msg_mag.header.stamp = msg.header.stamp

				if ser.isOpen():
					ii = 0
					text = ser.readline()
					#rospy.loginfo(text)
					output = text.split(", ")
					
				#	ii += 1
					
					# #Accelerometer
					# accelx = float(output[ii])
					# accely = float(output[ii + 1])
					# accelz = float(output[ii + 2])
					# ii += 3

					# #Gyro
					# gyrox = float(output[ii])
					# gyroy = float(output[ii + 1])
					# gyroz = float(output[ii + 2])
					# ii += 3
					
					# #Magnetometer
					magx = float(output[ii])
					magy = float(output[ii + 1])
					magz = float(output[ii + 2])
					ii += 3
					
					#Quaternion
					qw = float(output[ii])
					qx = float(output[ii + 1])
					qy = float(output[ii + 2])
					qz = float(output[ii + 3])
					ii += 4

					#Normalize quaternion
					d = math.sqrt(qw*qw + qx*qx + qy*qy + qz*qz)
					qx = qx / d
					qy = qy / d
					qz = qz / d
					qw = qw / d

					msg.orientation.x = qx
					msg.orientation.y = qy
					msg.orientation.z = qz
					msg.orientation.w = qw
					
					#Roll Pitch Yaw
					#From Quaternion
				#	quaternion = (
				#		qx,
				#		qy,
				#		qz,
				#		qw)
				#	euler = tf.transformations.euler_from_quaternion(quaternion)

				#	roll = euler[0]
				#	pitch = euler[1]
				#	yaw = euler[2]
					
					#rospy.loginfo(str(roll) + ", " + str(pitch) + ", " + str(yaw))
					#rospy.loginfo(str(qx) + ", " + str(qy) + ", " + str(qz) + ", " + str(qw))

					#From IMU readings									
					#pitch = float(output[ii])
					#roll = float(output[ii + 1])
					#yaw = float(output[ii + 2])
					#quaternion = tf.transformations.quaternion_from_euler(math.radians(roll), math.radians(pitch), math.radians(yaw))
					# msg.orientation.x = quaternion[0]
					# msg.orientation.y = quaternion[1]
					# msg.orientation.z = quaternion[2]
					# msg.orientation.w = quaternion[3]
					#rospy.loginfo(str(roll) + ", " + str(pitch) + ", " + str(yaw))
				#	ii += 3        
   
					#Heading
				#	head = float(output[ii])					
					
					#Calculate Heading from calibrated magnetometer values
					#Rotating the Robot in all directions will calibrate the magnetometer values
					if magx > maxmagx: maxmagx = magx
					if magx < minmagx: minmagx = magx
					if magy > maxmagy: maxmagy = magy
					if magy < minmagy: minmagy = magy
					if magz > maxmagz: maxmagz = magz
					if magz < minmagz: minmagz = magz

					calmagx = (magx - (maxmagx + minmagx) / 2) / (maxmagx - minmagx)
					calmagy = (magy - (maxmagy + minmagy) / 2) / (maxmagy - minmagy)
					calmagz = (magz - (maxmagz + minmagz) / 2) / (maxmagz - minmagz)
					heading1 = - math.atan2(calmagx, calmagy)
					#A simple rolling average filtering of heading values
					heading = round((heading + heading1) / 2, 2)

					heading -= math.radians(4.2) #Magnetic declination GREECE

					#Replace Yaw with Magnetic Heading
					quaternion = tf.transformations.quaternion_from_euler(0, 0, heading + math.pi/2)
					msg_mag.orientation.x  = quaternion[0]
					msg_mag.orientation.y  = quaternion[1]
					msg_mag.orientation.z  = quaternion[2]
					msg_mag.orientation.w  = quaternion[3]

					#rospy.loginfo(str(head) + ", " + str(math.degrees(heading)))
					ser.flushInput()

				pub.publish(msg)
				pub2.publish(msg_mag)
				rate.sleep()

			#To fix error on shutdown
			rospy.sleep(1)

		except Exception as e:
			#On error send diagnostics not ok
			diag_msg.status = [
				DiagnosticStatus(name = 'IMU', level = DiagnosticStatus.ERROR, message = 'Cannot Connect to IMU '+ str(e) , hardware_id = 'IMU')
			]
			diag_msg.header.stamp = rospy.Time.now()
			diagn.publish(diag_msg)
			rospy.logwarn("Cannot Connect to IMU " + str(e))
			rospy.sleep(2)
			

if __name__ == '__main__':
	try:
		imu_publish()
	except rospy.ROSInterruptException:
		pass
