#!/usr/bin/env python
import rospy
from math import *
from robot_core.msg import RobotStates
from geometry_msgs.msg import PoseStamped
from sensor_msgs.msg import BatteryState, NavSatFix, NavSatStatus
from diagnostic_msgs.msg import DiagnosticArray, DiagnosticStatus
from nav_msgs.msg import Path

#STATES
# 99 : Powered Off
# 1  : Self Diagnostics
# 2  : Energy Management
# 3  : Waiting for Destination
# 4  : Charging
# 5  : Moving to Point
# 6  : Inspecting
# 7  : Following Object

state = 0

saved_path = Path()

#Diagnistic report
diagn_imu = True
diagn_gps = True
diagn_motors = True
diagn_gps_fix = False

battery_voltage = 0
solar_voltage = 0

destination_defined = False
destination_reached = False
inspection_finished = False

INSPECTION_TIME = 10 #sec

#Threshold values
BATTERY_V1 = 24.5
BATTERY_V2 = 23.7
SOLAR_V = 25


def update_status(msg):
	global diagn_imu, diagn_gps, diagn_motors
	for component in msg.status:
		if component.name == 'IMU':
			if component.level == DiagnosticStatus.OK:
				diagn_imu = True
			else:
				diagn_imu = False
		elif component.name == 'GPS':
			if component.level == DiagnosticStatus.OK:
				diagn_gps = True
			else:
				diagn_gps = False
		elif component.name == 'Motors':
			if component.level == DiagnosticStatus.OK:
				diagn_motors = True
			else:
				diagn_motors = False

def update_position(msg):
	global diagn_gps_fix
	if msg.status.status == NavSatStatus.STATUS_FIX:
		diagn_gps_fix = True
	else:
		diagn_gps_fix = False

def update_battery(msg):
	global battery_voltage
	battery_voltage = msg.voltage
	battery_voltage = 25 # TESTING. DELETE in production

def update_solar_cell(msg):
	global solar_voltage
	solar_voltage = msg.voltage

def update_target(msg):
	global destination_defined
	destination_defined = True

def update_path(msg):
	global saved_path
	saved_path = msg

def destination_reached_state(msg):
	global destination_reached, state, current_point
	if msg.state == RobotStates.REACHED_POINT:
		destination_reached = True
	elif msg.state == RobotStates.MANUAL_MOVE:
		state = 0
	elif msg.state == RobotStates.EXECUTING_MOVE:
		state = 2

def update_robot_state(msg):
	global state
	state = msg.state

def path_planner():
	global current_point, current_position, target_position, state, destination_defined, destination_reached, inspection_finished
	state = 0
	previous_state = 0
	rospy.init_node('path_planner', anonymous=True)
	pub = rospy.Publisher('robot_state', RobotStates, queue_size=2)
	pub2 = rospy.Publisher("target_pose", PoseStamped, queue_size=2)
	rospy.Subscriber("diagnostics", DiagnosticArray, update_status)
	rospy.Subscriber("fix", NavSatFix, update_position)
	rospy.Subscriber("target_pose", PoseStamped, update_target)
	rospy.Subscriber("battery_state", BatteryState, update_battery)
	rospy.Subscriber("solar_cell_state", BatteryState, update_solar_cell)
	rospy.Subscriber("move_status", RobotStates, destination_reached_state)
	rospy.Subscriber("robot_state", RobotStates, update_robot_state)
	rospy.Subscriber("path", Path, update_path)
	msg = RobotStates()

	target_point = PoseStamped()
	current_point = 0
	inspection_starttime = 0

	rate = rospy.Rate(0.5)
	while not rospy.is_shutdown():
		change = False
		#STATE 0 Manual Move
		if state == 0:
			#rospy.loginfo(rospy.get_caller_id() + " Manual move")
			state = 0
			destination_defined = False

		#STATE 1 Self Diagnostics
		elif state == 1:
			#if diagn_motors and diagn_gps and diagn_gps_fix and diagn_imu:
			#if diagn_gps_fix and diagn_imu:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 1: Self Diagnostics")
				previous_state = state
			if diagn_imu: #TESTING, DELETE in production
				state = 2
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Self Diagnostics OK moving to State " + str(state))

		#STATE 2 Energy Management
		elif state == 2:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 2: Energy Management")
				previous_state = state
			if battery_voltage > BATTERY_V1:
				state = 3
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage OK moving to State " + str(state))
			elif battery_voltage < BATTERY_V1 and solar_voltage > SOLAR_V:
				state = 4
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage NOT OK moving to State " + str(state))
		
		#STATE 3 Waiting for Destination
		elif state == 3:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 3: Waiting for Destination")
				previous_state = state
			if battery_voltage < BATTERY_V2:
				state = 2
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage NOT OK moving to State " + str(state))
			elif destination_defined:
				state = 5
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Destination Received moving to State " + str(state))
			else:
				if len(saved_path.poses) > current_point and len(saved_path.poses) > 0:
					target_point.pose = saved_path.poses[current_point].pose
					target_point.header.frame_id = "earth"
					target_point.header.stamp = rospy.Time.now()
					pub2.publish(target_point)
				else:
					current_point = 0
	
		#STATE 4 Charging
		elif state == 4:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 3: Charging")
				previous_state = state
			if battery_voltage < BATTERY_V1 and solar_voltage < SOLAR_V:
				state = 2
				change = True
				rospy.loginfo(rospy.get_caller_id() + " No Solar Voltage moving to State " + str(state))
			elif battery_voltage > BATTERY_V1:
				state = 3
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage OK moving to State " + str(state))
		
		#STATE 5 Moving to Point
		elif state == 5:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 5: Moving to Point")
				previous_state = state
			if destination_reached:
				state = 6
				change = True
				destination_reached = False
				destination_defined = False
				#Move to next point
				current_point += 1
				rospy.loginfo(rospy.get_caller_id() + " Destination Reached moving to State " + str(state))
			elif battery_voltage < BATTERY_V2:
				state = 2
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage NOT OK moving to State " + str(state))

		#STATE 6 Inpecting
		elif state == 6:
			if previous_state != state: 
				rospy.loginfo(rospy.get_caller_id() + " State 6: Inspecting POI")
				previous_state = state
			if inspection_finished:
				state = 3
				change = True
				inspection_starttime = 0
				inspection_finished = False
				rospy.loginfo(rospy.get_caller_id() + " Inspection Finished moving to State " + str(state))
			elif battery_voltage < BATTERY_V2:
				state = 2
				change = True
				rospy.loginfo(rospy.get_caller_id() + " Battery Voltage NOT OK moving to State " + str(state))
			else:
				if inspection_starttime == 0:
					rospy.loginfo(rospy.get_caller_id() + " Inspecting for " + str(INSPECTION_TIME) + " seconds... ")
					inspection_starttime = rospy.Time.now().to_sec()
				else:
					if rospy.Time.now().to_sec() - inspection_starttime > INSPECTION_TIME:
						inspection_finished = True
		
		if change == True:
			msg.header.stamp = rospy.Time.now()
			msg.state = state
			pub.publish(msg)
			#rospy.loginfo(rospy.get_caller_id() + " State: " + str(state))

		#rospy.sleep(0.1)
		rate.sleep()

	#To fix error on shutdown
	rospy.sleep(1)	

	#rospy.spin()



if __name__ == '__main__':
	try:
		path_planner()
	except rospy.ROSInterruptException:
		pass
