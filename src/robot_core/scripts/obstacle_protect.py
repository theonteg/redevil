#!/usr/bin/env python
import rospy
from robot_core.msg import MotorControl
from sensor_msgs.msg import Range

FORWARD = 1
STOP = 0
BACKWARD = -1
MINIMUM_DISTANCE = 0.4


def refresh_speed(data):
	global left_motor_current_speed
	global left_motor_current_direction
	global right_motor_current_speed
	global right_motor_current_direction
	if data.text == "Keyboard Input":
		left_motor_current_speed = data.left_motor_speed
		left_motor_current_direction = data.left_motor_direction
		right_motor_current_speed = data.right_motor_speed
		right_motor_current_direction = data.right_motor_direction

def check_center(data):
	if data.range < MINIMUM_DISTANCE:
		if (left_motor_current_direction == FORWARD) and (right_motor_current_direction == FORWARD):
			pub = rospy.Publisher('motors_movement', MotorControl, queue_size=10)
			msg = MotorControl()
			msg.text = "Automatic Input Obstacle Protect"
			msg.header.stamp = rospy.Time.now()
			msg.left_motor_speed = 0
			msg.left_motor_direction = left_motor_current_direction
			msg.right_motor_speed = 0
			msg.right_motor_direction = right_motor_current_direction
			#pub.publish(msg)


def listener():
	global left_motor_current_speed
	global left_motor_current_direction
	global right_motor_current_speed
	global right_motor_current_direction
	left_motor_current_speed = 0
	left_motor_current_direction = STOP
	right_motor_current_speed = 0
	right_motor_current_direction = STOP

	rospy.init_node('obstacle_protect', anonymous=True)

	rospy.Subscriber("motors_movement", MotorControl, refresh_speed)
	rospy.Subscriber("ultrasonic_sensor_front_center", Range, check_center)
	# spin() simply keeps python from exiting until this node is stopped
	rospy.spin()


if __name__ == '__main__':
	listener()
