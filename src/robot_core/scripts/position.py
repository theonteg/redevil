#!/usr/bin/env python
import rospy
from math import isnan
#import std_msgs.msg
from geodesy.utm import fromMsg
from geometry_msgs.msg import Pose, PoseStamped, TransformStamped
from sensor_msgs.msg import NavSatFix, Imu
from geographic_msgs.msg import GeoPoint, GeoPose
# For transformations
#import tf
import tf2_ros
#import geometry_msgs.msg

current_pose = PoseStamped()
current_pose.pose.position.x = 0.0
current_pose.pose.position.y = 0.0
current_pose.pose.position.z = 0.0
current_pose.pose.orientation.x = 0.0
current_pose.pose.orientation.y = 0.0
current_pose.pose.orientation.z = 0.0
current_pose.pose.orientation.w = 1.0
current_geopose = GeoPose()
first_pose = PoseStamped()
first_pose.pose.position.x = 0.0
first_pose.pose.position.y = 0.0
first_pose.pose.position.z = 0.0
first_pose.pose.orientation.x = 0.0
first_pose.pose.orientation.y = 0.0
first_pose.pose.orientation.z = 0.0
first_pose.pose.orientation.w = 1.0
pub = rospy.Publisher('pose', PoseStamped, queue_size=1)
pub2 = rospy.Publisher('geopose', GeoPose, queue_size=1)
is_first_pose = True
br = tf2_ros.TransformBroadcaster()

def transform_first_pose():
  t = TransformStamped()
  t.header.stamp = rospy.Time.now()
  t.header.frame_id = "earth"
  t.child_frame_id = "map"
  t.transform.translation.x = first_pose.pose.position.x
  t.transform.translation.y = first_pose.pose.position.y
  t.transform.translation.z = 0.0
  t.transform.rotation.x = 0.0
  t.transform.rotation.y = 0.0
  t.transform.rotation.z = 0.0
  t.transform.rotation.w = 1.0
  br.sendTransform(t)

def transform_pose():
  t = TransformStamped()
  t.header.stamp = rospy.Time.now()
  t.header.frame_id = "earth"
  t.child_frame_id = "base_link"
  t.transform.translation.x = current_pose.pose.position.x
  t.transform.translation.y = current_pose.pose.position.y
  t.transform.translation.z = 0.0
  t.transform.rotation.x = current_pose.pose.orientation.x
  t.transform.rotation.y = current_pose.pose.orientation.y
  t.transform.rotation.z = current_pose.pose.orientation.z
  t.transform.rotation.w = current_pose.pose.orientation.w
  br.sendTransform(t)

def update_position(msg):
  global current_pose, current_geopose, first_pose, is_first_pose
  gpoint = GeoPoint()
  if isnan(msg.latitude):
    gpoint.latitude = 35.5264213333 #0.0
  else:
    gpoint.latitude = msg.latitude
  if isnan(msg.longitude):
    gpoint.longitude = 24.0690333 #0.0
  else:
    gpoint.longitude = msg.longitude
  if isnan(msg.altitude):
    gpoint.altitude = 0.0
  else:
    gpoint.altitude = msg.altitude
  utmpoint = fromMsg(gpoint)
  current_pose.pose.position.x = utmpoint.easting
  current_pose.pose.position.y = utmpoint.northing
  current_pose.pose.position.z = utmpoint.altitude
  current_geopose.position = gpoint
  pub2.publish(current_geopose)
  #publish_pose()
  if is_first_pose:
    first_pose.pose.position.x = current_pose.pose.position.x
    first_pose.pose.position.y = current_pose.pose.position.y
    first_pose.pose.position.z = current_pose.pose.position.z
    is_first_pose = False
  # transform_first_pose()

def update_orientation(msg):
  global current_pose, current_geopose
  current_pose.pose.orientation = msg.orientation
  current_geopose.orientation = msg.orientation
  # transform_pose()
  # transform_first_pose()
  publish_pose()

def publish_pose():
  global current_pose, current_geopose
  current_pose.header.stamp = rospy.Time.now()
  current_pose.header.frame_id = "base_link"
  pub.publish(current_pose)

def position():
  global pose_list
  rospy.init_node('robot_pose', anonymous=True)
  rospy.Subscriber("fix", NavSatFix, update_position)
  rospy.Subscriber("imu", Imu, update_orientation)
  #using magnetometer orientation
  #rospy.Subscriber("imu_mag", Imu, update_orientation)

  rate = rospy.Rate(5)
  while not rospy.is_shutdown():
    transform_pose()
    transform_first_pose()    


    rate.sleep()



  rospy.spin()


if __name__ == '__main__':
  try:
    position()
  except rospy.ROSInterruptException:
    pass
