#!/usr/bin/env python
import rospy
import Adafruit_BBIO.UART as UART
from geometry_msgs.msg import Twist
import serial


SABERTOOTH_PIN =  "UART1" #"UART1"
UART.setup(SABERTOOTH_PIN)
SABERTOOTH_PORT = "/dev/ttyO1" #"/dev/ttyO1"

ser = serial.Serial(port = SABERTOOTH_PORT, baudrate=9600)
ser.close()

ACCELERATION_RAMP_TIME = 1
ACCELERATION_STEPS = 10
MAX_WHEEL_VELOCITY = 15 #rad/sec
WHEEL_DISTANCE = 1.0 # 0.335 #wheel distance
WHEEL_RADIUS = 0.12 #wheel radius

current_velocity = Twist()
current_velocity.linear.x = 0
current_velocity.linear.y = 0
current_velocity.linear.z = 0
current_velocity.angular.x = 0
current_velocity.angular.y = 0
current_velocity.angular.z = 0
left_motor_current_speed = 0
right_motor_current_speed = 0

ser.open()
if ser.isOpen():
    ser.write(chr(0))

def set_speed(left_speed, right_speed):
    if ser.isOpen():
        lefttemp = int(64 + left_speed * 63 / 100)
        righttemp = int(192 + right_speed * 63 / 100)
        #DISABLE 0 if there is low responsiveness from motors.
        # if lefttemp == 64 and righttemp == 192:
        #     ser.write(chr(0))
        # else:
        #     ser.write(chr(lefttemp))
        #     rospy.sleep(0.01)
        #     ser.write(chr(righttemp))
        ser.write(chr(lefttemp))
        rospy.sleep(0.01)
        ser.write(chr(righttemp))

def acceleration_ramp(left_start_speed, left_stop_speed, right_start_speed, right_stop_speed):
    for x in range(0, ACCELERATION_STEPS):
        rospy.sleep(ACCELERATION_RAMP_TIME / ACCELERATION_STEPS)
        left_speed = ((left_stop_speed - left_start_speed) / ACCELERATION_STEPS ) * (x + 1) + left_start_speed
        right_speed = ((right_stop_speed - right_start_speed) / ACCELERATION_STEPS ) * (x + 1) + right_start_speed
        set_speed(left_speed * 100, right_speed * 100)

def move_motors(data):
    global current_velocity, left_motor_current_speed, right_motor_current_speed
    left_speed = (data.linear.x - (WHEEL_DISTANCE / 2) * data.angular.z) / WHEEL_RADIUS
    right_speed = (data.linear.x + (WHEEL_DISTANCE / 2) * data.angular.z) / WHEEL_RADIUS
    if left_speed > MAX_WHEEL_VELOCITY: 
        left_speed = MAX_WHEEL_VELOCITY
    elif left_speed < - MAX_WHEEL_VELOCITY:
        left_speed = - MAX_WHEEL_VELOCITY
    if right_speed > MAX_WHEEL_VELOCITY:
        right_speed = MAX_WHEEL_VELOCITY
    elif right_speed < - MAX_WHEEL_VELOCITY:
        right_speed = - MAX_WHEEL_VELOCITY
    left_speed_percent = left_speed / MAX_WHEEL_VELOCITY
    right_speed_percent = right_speed / MAX_WHEEL_VELOCITY
    acceleration_ramp(left_motor_current_speed, left_speed_percent, right_motor_current_speed, right_speed_percent)
    left_motor_current_speed = left_speed_percent
    right_motor_current_speed = right_speed_percent
    rospy.loginfo(rospy.get_caller_id() + " L Motor Speed %: " + str(round(left_motor_current_speed * 100, 0)) + " R Motor Speed %: " + str(round(right_motor_current_speed * 100, 0)))

def listener():
    rospy.init_node('wheel_controller', anonymous=True)
    rospy.Subscriber("cmd_vel", Twist, move_motors)

    rospy.spin()

    rospy.on_shutdown(myhook)

def myhook():
    if ser.isOpen():
        ser.write(chr(0))
        ser.close()
    
    #To fix error on shutdown
    rospy.sleep(1)


if __name__ == '__main__':
    try:
        listener()
    except rospy.ROSInterruptException:
        pass

