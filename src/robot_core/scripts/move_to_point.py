#!/usr/bin/env python
import rospy
import tf
from math import *
from robot_core.msg import RobotStates
from geometry_msgs.msg import Pose, PoseStamped, Twist
from sensor_msgs.msg import NavSatFix, Imu

DISTANCE_ACCURACY = 0.8 #meters from target to stop
ANGLE_ACCURACY = 10.0 * pi / 180.0 #rad from target to stop
MOVE_LINEAR_VELOCITY = 1.0 #m/sec (1.4m/s (5km/h) walking speed, 2.5m/s (9km/h) fast walking speed)
MOVE_ANGULAR_VELOCITY_STATIC = pi / 2 #rad/sec
MOVE_ANGULAR_VELOCITY = pi / 5 #rad/sec

WAIT_TIME_IN_POI = 10 #sec

execute_move = False
target_pose = Pose()
current_pose = Pose()
velocity = Twist()
velocity.linear.x = 0
velocity.linear.y = 0
velocity.linear.z = 0
velocity.angular.x = 0
velocity.angular.y = 0
velocity.angular.z = 0
rob_state = 0
distance_from_target = 0
angle_between_coords = 0
angle_from_target_point = 1.0
target_orientation = 0.0
angle_from_target_orientation = 1.0
destination_reached = False
pub = rospy.Publisher('cmd_vel', Twist, queue_size=2)
pub2 = rospy.Publisher('move_status', RobotStates, queue_size=2)

sample_rate = 25

def distanceBetweenPoints(pose1, pose2):
	return round(sqrt(pow(pose2.position.x - pose1.position.x, 2) + pow(pose2.position.y - pose1.position.y, 2)), 2)

def angleBetweenPoints(pose1, pose2):
	return round(atan2((pose2.position.y - pose1.position.y), (pose2.position.x - pose1.position.x)), 4)

def angleFromTarget(currentAngle, targetAngle):
	angle = (targetAngle - currentAngle)
	if angle >= pi:
		angle = angle - 2 * pi
	elif angle <= -pi:
		angle = angle + 2 * pi
	return angle

def update_target(point):
	global target_pose, distance_from_target, angle_between_coords, target_orientation, destination_reached
	target_pose = point.pose
	distance_from_target = distanceBetweenPoints(current_pose, target_pose)
	angle_between_coords = angleBetweenPoints(current_pose, target_pose)
	quaternion = (
		target_pose.orientation.x,
		target_pose.orientation.y,
		target_pose.orientation.z,
		target_pose.orientation.w)
	euler = tf.transformations.euler_from_quaternion(quaternion)
	target_orientation = euler[2]
	#rospy.loginfo(rospy.get_caller_id() + " Received target pose: y: " + str(target_pose.position.y) + " \t x: " + str(target_pose.position.x) )
	rospy.loginfo(rospy.get_caller_id() + " Received target pose (relative): y: " + str(round(target_pose.position.y - current_pose.position.y,1)) + " m \t x: " + str(round(target_pose.position.x - current_pose.position.x, 1)) + " m")
	rospy.loginfo(rospy.get_caller_id() + " Distance from target: " + str(distance_from_target) + " m, Angle from target: " + str(round(degrees(angle_between_coords),1)) + " deg")
	destination_reached = False
	
def update_position(position):
	global current_pose, distance_from_target, angle_between_coords, angle_from_target_point, angle_from_target_orientation
	current_pose = position.pose
	quaternion = (
		current_pose.orientation.x,
		current_pose.orientation.y,
		current_pose.orientation.z,
		current_pose.orientation.w)
	euler = tf.transformations.euler_from_quaternion(quaternion)
	yaw = euler[2]
	angle_from_target_point = angleFromTarget(yaw, angle_between_coords)	
	angle_from_target_orientation = angleFromTarget(yaw, target_orientation)
	distance_from_target = distanceBetweenPoints(current_pose, target_pose)
	angle_between_coords = angleBetweenPoints(current_pose, target_pose)

def check_state(state):
	global execute_move, velocity
	if state.state == 5: #Moving to Point
		execute_move = True
	#elif state.state == state.MANUAL_MOVE:
	elif state.state == 7: #Follow Object
		execute_move = False
	else:
		execute_move = False
		if velocity.linear.x !=0 or velocity.angular.z !=0:
			velocity.linear.x = 0
			velocity.angular.z = 0
			pub.publish(velocity)
	
def move_to_point():
	global execute_move, velocity, destination_reached
	rospy.init_node('move_to_point', anonymous=True)
	rospy.Subscriber("robot_state", RobotStates, check_state)
	rospy.Subscriber("pose", PoseStamped, update_position)
	rospy.Subscriber("target_pose", PoseStamped, update_target)

	rate = rospy.Rate(sample_rate)
	
	while not rospy.is_shutdown():
		if execute_move == True:
			#Calculate the distance from the target point, if it is greater than the DISTANCE_ACCURACY
			if distance_from_target > DISTANCE_ACCURACY:

				if abs(angle_from_target_point) > ANGLE_ACCURACY:
					#If angle is larger than ANGLE_ACCURACY stop and turn
					if angle_from_target_point < 0:
						if velocity.angular.z >= 0.0 or (velocity.linear.x != 0 and velocity.angular.z < 0):
							#Turn Right
							velocity.linear.x = 0
							velocity.angular.z = - MOVE_ANGULAR_VELOCITY_STATIC
							pub.publish(velocity)
					elif angle_from_target_point > 0:
						if velocity.angular.z <= 0.0 or (velocity.linear.x != 0 and velocity.angular.z > 0):
							#Turn Left
							velocity.linear.x = 0
							velocity.angular.z = MOVE_ANGULAR_VELOCITY_STATIC
							pub.publish(velocity)
				elif abs(angle_from_target_point) > ANGLE_ACCURACY / 2 and velocity.linear.x != 0:
					#If angle is larger than ANGLE_ACCURACY/2 and robot is moving forward then turn while moving. 
					if angle_from_target_point < 0:
						if velocity.angular.z >= 0.0:
							#Turn Right
							velocity.angular.z = - MOVE_ANGULAR_VELOCITY
							pub.publish(velocity)
					elif angle_from_target_point > 0:
						if velocity.angular.z <= 0.0:
							#Turn Left
							velocity.angular.z = MOVE_ANGULAR_VELOCITY
							pub.publish(velocity)
				else:
					if not ((angle_from_target_point < 0 and velocity.angular.z < 0.0) or (angle_from_target_point > 0 and velocity.angular.z > 0.0)):
						if velocity.linear.x == 0:
							velocity.linear.x = MOVE_LINEAR_VELOCITY
						if velocity.angular.z != 0:
							velocity.angular.z = 0
							pub.publish(velocity)

			#If you reached target point
			else:
				#CHECK if move to angle works ok
				#move_to_angle(angleFromTarget(current_pose.orientation.z, target_pose.orientation.z))
				if velocity.linear.x != 0:
					velocity.linear.x = 0
					velocity.angular.z = 0
					pub.publish(velocity)
				else:
					if abs(angle_from_target_orientation) > ANGLE_ACCURACY:
						if angle_from_target_orientation < 0:
							if velocity.angular.z >= 0.0:
								#Turn Right
								velocity.angular.z = - MOVE_ANGULAR_VELOCITY_STATIC
								pub.publish(velocity)
						elif angle_from_target_orientation > 0:
							if velocity.angular.z <= 0.0:
								#Turn Left
								velocity.angular.z = MOVE_ANGULAR_VELOCITY_STATIC
								pub.publish(velocity)
					else:
						if not ((angle_from_target_orientation < 0 and velocity.angular.z < 0.0) or (angle_from_target_orientation > 0 and velocity.angular.z > 0.0)):
							if velocity.angular.z != 0:
								velocity.angular.z = 0
								pub.publish(velocity)

							if not destination_reached:
								state = RobotStates()
								state.header.stamp = rospy.Time.now()
								state.state = state.REACHED_POINT
								pub2.publish(state)
								destination_reached = True
		rate.sleep()

	rospy.spin()

	#To fix error on shutdown
	rospy.sleep(1)	



if __name__ == '__main__':
	try:
		move_to_point()
	except rospy.ROSInterruptException:
		pass

