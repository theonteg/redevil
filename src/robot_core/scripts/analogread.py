#!/usr/bin/env python
import rospy
import std_msgs.msg
import math
import Adafruit_BBIO.ADC as ADC
import Adafruit_BBIO.GPIO as GPIO
from sensor_msgs.msg import BatteryState
from sensor_msgs.msg import Range

ANALOG_READ_PIN = "P9_33"
MUX_PIN_S0 = "P9_23"
MUX_PIN_S1 = "P9_25"
MUX_PIN_S2 = "P9_12"
SONAR_TRIGER_PIN = "P9_30"

GPIO.setup(MUX_PIN_S0, GPIO.OUT)
GPIO.setup(MUX_PIN_S1, GPIO.OUT)
GPIO.setup(MUX_PIN_S2, GPIO.OUT)
GPIO.setup(SONAR_TRIGER_PIN, GPIO.OUT)
ADC.setup()

#converts input analog voltage to meters
def convert_to_range(value):
	vcc = 4.92
	return round(((value * 1.81 * 512) / vcc ) * 0.0254, 2)

def gpio_out(s0, s1, s2):
	if s0 == 1:
		GPIO.output(MUX_PIN_S0,GPIO.HIGH)
	else:
		GPIO.output(MUX_PIN_S0,GPIO.LOW)
	if s1 == 1:
		GPIO.output(MUX_PIN_S1,GPIO.HIGH)
	else:
		GPIO.output(MUX_PIN_S1,GPIO.LOW)
	if s2 == 1:
		GPIO.output(MUX_PIN_S2,GPIO.HIGH)
	else:
		GPIO.output(MUX_PIN_S2,GPIO.LOW)

def talker():
	rospy.init_node('analog_read', anonymous=True)
	rate = rospy.Rate(4)
	pub = rospy.Publisher('ultrasonic_sensor_front_left', Range, queue_size=2)
	pub1 = rospy.Publisher('ultrasonic_sensor_front_center', Range, queue_size=2)
	pub2 = rospy.Publisher('ultrasonic_sensor_front_right', Range, queue_size=2)
	pub3 = rospy.Publisher('ultrasonic_sensor_back_center', Range, queue_size=2)
	pub4 = rospy.Publisher('battery_state', BatteryState, queue_size=2)
	pub5 = rospy.Publisher('solar_cell_state', BatteryState, queue_size=2)
	msg_range1 = Range()
	msg_range1.header.frame_id = "ultrasonic_front_left"
	msg_range1.radiation_type = msg_range1.ULTRASOUND
	msg_range1.field_of_view = math.pi/6
	msg_range1.min_range = 0.152 #m
	msg_range1.max_range = 6.45  #m
	msg_range2 = Range()
	msg_range2.header.frame_id = "ultrasonic_front_center"
	msg_range2.radiation_type = msg_range2.ULTRASOUND
	msg_range2.field_of_view = math.pi/6
	msg_range2.min_range = 0.152 #m
	msg_range2.max_range = 6.45  #m
	msg_range3 = Range()
	msg_range3.header.frame_id = "ultrasonic_front_right"
	msg_range3.radiation_type = msg_range3.ULTRASOUND
	msg_range3.field_of_view = math.pi/6
	msg_range3.min_range = 0.152 #m
	msg_range3.max_range = 6.45  #m
	msg_range4 = Range()
	msg_range4.header.frame_id = "ultrasonic_back_center"
	msg_range4.radiation_type = msg_range4.ULTRASOUND
	msg_range4.field_of_view = math.pi/6
	msg_range4.min_range = 0.152 #m
	msg_range4.max_range = 6.45  #m
	msg1 = BatteryState()
	msg2 = BatteryState()
	last_battery_check = rospy.Time.now()
				
	while not rospy.is_shutdown():
		#Y0, 000 ULTRASONIC SENSOR FRONT LEFT
		gpio_out(0,0,0)
		rospy.sleep(0.01)
		msg_range1.header.stamp = rospy.Time.now()
		msg_range1.range = convert_to_range(ADC.read(ANALOG_READ_PIN))
		pub.publish(msg_range1)
		#Y1, 100 ULTRASONIC SENSOR FRONT CENTER
		gpio_out(1,0,0)
		rospy.sleep(0.01)
		msg_range2.header.stamp = rospy.Time.now()
		msg_range2.range = convert_to_range(ADC.read(ANALOG_READ_PIN))
		pub1.publish(msg_range2)
		#Y2, 010 ULTRASONIC SENSOR FRONT RIGHT
		gpio_out(0,1,0)
		rospy.sleep(0.01)
		msg_range3.header.stamp = rospy.Time.now()
		msg_range3.range = convert_to_range(ADC.read(ANALOG_READ_PIN))
		pub2.publish(msg_range3)
		#Y3, 110 ULTRASONIC SENSOR BACK CENTER
		gpio_out(1,1,0)
		rospy.sleep(0.01)
		msg_range4.header.stamp = rospy.Time.now()
		msg_range4.range = convert_to_range(ADC.read(ANALOG_READ_PIN))
		pub3.publish(msg_range4)

		if rospy.Time.now() - last_battery_check > rospy.Duration.from_sec(1):
			#Y4, 001 BATTERY STATE
			gpio_out(0,0,1)
			rospy.sleep(0.01)
			msg1.header.stamp = rospy.Time.now()
			msg1.voltage = round((ADC.read(ANALOG_READ_PIN)*1.81*(9970+558))/558, 1)
			msg1.present = 1
			#Read Current
			#Y6 110 BATTERY CURRENT
			gpio_out(0,1,1)
			rospy.sleep(0.01)
			#msg1.current = round((ADC.read(ANALOG_READ_PIN) * 1.8 * 2 - 2.5)/0.066,3)
			msg1.current = round(((1.230 - ADC.read(ANALOG_READ_PIN) * 1.81)/0.066) * 2,3)
			pub4.publish(msg1)
			#Y5, 101 SOLAR CELLS STATE
			gpio_out(1,0,1)
			rospy.sleep(0.01)
			msg2.header.stamp = rospy.Time.now()
			msg2.voltage = round((ADC.read(ANALOG_READ_PIN)*1.81*(9970+558))/558, 1)
			msg2.present = 1
			pub5.publish(msg2)
			last_battery_check = rospy.Time.now()

		#Triger the sonars
		GPIO.output(SONAR_TRIGER_PIN,GPIO.HIGH)
		rospy.sleep(0.001)
		GPIO.output(SONAR_TRIGER_PIN,GPIO.LOW)
		#rospy.sleep(0.2)
		
		rate.sleep()
		
	#To fix error on shutdown
	rospy.sleep(1)

if __name__ == '__main__':
	try:
		talker()
	except rospy.ROSInterruptException: pass
