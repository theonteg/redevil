#!/usr/bin/env python
import rospy
from math import *
from robot_core.msg import RobotStates
from geometry_msgs.msg import Twist
from sensor_msgs.msg import RegionOfInterest
from pixy_msgs.msg import PixyData

MOVE_LINEAR_VELOCITY = 0.8 #m/sec (1.4m/s (5km/h) walking speed, 2.5m/s (9km/h) fast walking speed)
MOVE_ANGULAR_VELOCITY_STATIC = pi / 2.5 #rad/sec
MOVE_ANGULAR_VELOCITY = pi / 3.5 #rad/sec

execute_move = False
velocity = Twist()
target_object_found = False
no_target_found_counter = 0
target_object = RegionOfInterest()
MAX_X = 320
MAX_Y = 200
velocity.linear.x = 0
velocity.linear.y = 0
velocity.linear.z = 0
velocity.angular.x = 0
velocity.angular.y = 0
velocity.angular.z = 0

pub = rospy.Publisher('cmd_vel', Twist, queue_size=2)

def check_state(state):
	global execute_move, velocity
	if state.state == 7: #Follow Object
		execute_move = True
	elif state.state == 5: #Move to Point
		execute_move = False
	else:
		execute_move = False
		if velocity.linear.x !=0 or velocity.angular.z !=0:
			velocity.linear.x = 0
			velocity.angular.z = 0
			pub.publish(velocity)

def update_target(msg):
	global target_object, target_object_found, no_target_found_counter
	target_object_found = False
	max_object = 0
	if len(msg.blocks) > 0:
		for i in range(0, len(msg.blocks)):
			if msg.blocks[i].signature == 1:
				#Searching for the largest object to avoid noise
				if max_object < msg.blocks[i].roi.width * msg.blocks[i].roi.height:
					target_object_found = True
					target_object = msg.blocks[i].roi
					max_object = msg.blocks[i].roi.width * msg.blocks[i].roi.height
	
	#To fix problem where randomly one message is empty
	if target_object_found == False:
		no_target_found_counter += 1
	else:
		no_target_found_counter = 0
	if no_target_found_counter <= 1:
		target_object_found = True
	
def follow_object():
	global execute_move, velocity
	rospy.init_node('follow_object', anonymous=True)
	rospy.Subscriber("robot_state", RobotStates, check_state)
	rospy.Subscriber("/my_pixy/block_data", PixyData, update_target)

	rate = rospy.Rate(5)
	
	while not rospy.is_shutdown():
		if execute_move == True and target_object_found == True:

			# rospy.loginfo(rospy.get_caller_id() + " Width: " + str(target_object.width) + " Height: " + str(target_object.height))
			# rospy.loginfo(rospy.get_caller_id() + " X: " + str( target_object.x_offset - MAX_X / 2 ) + " Y: " + str(target_object.y_offset - MAX_Y / 2))

			if target_object.width < 50 and target_object.height < 50:
				#Move towards object
				if velocity.linear.x <= 0:
					velocity.linear.x = MOVE_LINEAR_VELOCITY
					pub.publish(velocity)
			elif target_object.width > 60 and target_object.height > 60:
				#Move away from object
				if velocity.linear.x >= 0:
					velocity.linear.x = - MOVE_LINEAR_VELOCITY
					pub.publish(velocity)
			else:
				#Stop
				if velocity.linear.x != 0:
					velocity.linear.x = 0
					pub.publish(velocity)

			if (target_object.x_offset - MAX_X/2) < - 50:
				#Turn Left
				if velocity.angular.z <= 0:
					if velocity.linear.x != 0:
						velocity.angular.z = MOVE_ANGULAR_VELOCITY
					else:
						velocity.angular.z = MOVE_ANGULAR_VELOCITY_STATIC
					pub.publish(velocity)
			elif (target_object.x_offset - MAX_X/2) > 50:
				#Turn Right
				if velocity.angular.z >= 0:
					if velocity.linear.x != 0:
						velocity.angular.z = - MOVE_ANGULAR_VELOCITY
					else:
						velocity.angular.z = - MOVE_ANGULAR_VELOCITY_STATIC
					pub.publish(velocity)
			else:
				#Stop
				if velocity.angular.z != 0:
					velocity.angular.z = 0
					pub.publish(velocity)
		else:
			if velocity.linear.x != 0 or velocity.angular.z != 0:
				if target_object_found == False and execute_move == True:
					rospy.loginfo(rospy.get_caller_id() + " Target object Lost")
				velocity.linear.x = 0
				velocity.angular.z = 0
				pub.publish(velocity)

		rate.sleep()

	rospy.spin()

	#To fix error on shutdown
	rospy.sleep(1)	



if __name__ == '__main__':
	try:
		follow_object()
	except rospy.ROSInterruptException:
		pass

