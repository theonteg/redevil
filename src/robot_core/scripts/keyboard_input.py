#!/usr/bin/env python
import rospy
import tf
import curses
from math import *
from geodesy.utm import *
from std_msgs.msg import String
from robot_core.msg import RobotStates
from geometry_msgs.msg import Pose, PoseStamped, PoseArray, Twist, TwistStamped
from sensor_msgs.msg import BatteryState, NavSatFix, Imu
from pixy_msgs.msg import PixyData
from geographic_msgs.msg import GeoPose
from nav_msgs.msg import Path

MOVE_LINEAR_VELOCITY = 2.0 #m/sec (1.4m/s (5km/h) walking speed, 2.5m/s (9km/h) fast walking speed)
MOVE_ANGULAR_VELOCITY = pi  #rad/sec
MAX_WHEEL_VELOCITY = 15.0 #rad/sec
WHEEL_DISTANCE = 1.0 #0.335 #wheel distance
WHEEL_RADIUS = 0.12 #wheel radius

battery_voltage = 1.0
battery_current = 0.0
solar_voltage = 1.0
distance_from_target = 0.0
angle_from_target_point = 0.0
angle_from_target_orientation = 0.0
target_orientation = 0.0
no_target_found_counter = 0
yaw = 0.0
yaw_mag = 0.0
current_pose = Pose()
current_geopose = GeoPose()
saved_pose = Pose()
saved_geopose = GeoPose()
posestamped_list = []
pose_list = []
geopose_list = []
current_velocity = Twist()
gps_velocity = 0.0

speed_percent = 0.5
angular_correction_percent = 0.0
SPEED_PERCENT_STEP = 0.05
ANGULAR_CORRECTION_STEP = 0.1

state = 0

screen = curses.initscr()
curses.noecho()
curses.curs_set(0)
#curses.halfdelay(5)
screen.nodelay(1)
screen.keypad(1)
screen.addstr("Keyboard control for reDevil\n"
"U, 8: Forward\t\tM, 2: Backward\t\tH, 4: Left\t\tK ,6: Right\n"
"J, 5: Stop\t\tY, +: Speed++\t\tI, -: Speed--\n"
"N, 1: AngularSpeed++\t,, 3: AngularSpeed--\n"
"A: Send Poses\t\tS: Save Pose \t\tD: Delete Poses\n"
"F: Read Poses from File\tG: Save Poses to File\tR: Reset Trajectory\n"
"Z: Manual Move \t\tX: Move to Last Pose\tC: Automatic\n\n")

screen.addstr(8,0,"Linear Vel: 0.0 m/s \tAngular Vel: 0.0 rad/s          ")
screen.addstr(9,0,"Speed %: 0 \t\tAngular %: 0         ")
screen.addstr(11,0,"Battery: 0.0 V \t0.0 A \t0.0 W   ")
screen.addstr(12,0,"Solar: 0.0 V   ")
screen.addstr(14,0,"Longitude: 0.0          ")
screen.addstr(15,0,"Latitude:  0.0          ")
screen.addstr(16,0,"Altitude:  0.0          ")
screen.addstr(14,30,"Longitude: 0.0            ")
screen.addstr(15,30,"Latitude:  0.0            ")
screen.addstr(16,30,"Altitude:  0.0            ")
screen.addstr(17,0,"Roll:  0.0             ")
screen.addstr(18,0,"Pitch: 0.0             ")
screen.addstr(19,0,"Yaw:   0.0          ")
screen.addstr(20,0,"Yaw_mag:   0.0          ")
screen.addstr(18,30,"Target Object     X:   0.0       Y: 0.0           ")
screen.addstr(19,30,"Target Object Width:   0.0  Height: 0.0           ")
screen.addstr(22,0,"Distance from Target: 0.0 m        ")
screen.addstr(22,45,"GPS Velocity: 0.0 m/s        ")
screen.addstr(23,0,"Angle from Target Point: 0.0 deg     ")
screen.addstr(24,0,"Angle from Target Orientation: 0.0 deg     ")
screen.addstr(14,60, "Poses Saved: 0     ")       
screen.addstr(25,0,"State:              ")

def distanceBetweenPoints(pose1, pose2):
   return sqrt(pow(pose2.position.x - pose1.position.x, 2) + pow(pose2.position.y - pose1.position.y, 2))

def angleBetweenPoints(pose1, pose2):
   return degrees(atan2((pose2.position.y - pose1.position.y), (pose2.position.x - pose1.position.x)))

def update_battery(msg):
   global battery_voltage, battery_current
   battery_voltage = msg.voltage
   battery_current = msg.current
   screen.addstr(11,0,"Battery: " + str('{0:.1f}'.format(battery_voltage)) + " V \t" + str('{0:.3f}'.format(battery_current)) + " A \t " + str('{0:.1f}'.format(battery_voltage * battery_current)) + " W     ")

def update_solar(msg):
   global solar_voltage
   solar_voltage = msg.voltage
   screen.addstr(12,0,"Solar: " + str('{0:.1f}'.format(solar_voltage)) + " V     ")

def update_imu_mag(msg):
   global yaw_mag
   euler = tf.transformations.euler_from_quaternion((msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w))
   yaw_mag = degrees(euler[2])

def update_pose(msg):
   global current_pose, yaw, current_geopoint
   current_pose.orientation = msg.pose.orientation
   euler = tf.transformations.euler_from_quaternion((msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w))
   yaw = degrees(euler[2])
   current_pose.position = msg.pose.position
   update_distance_from_target()
   update_angle_from_target()
   screen.addstr(17,0,"Roll:  " + str('{0:.1f}'.format(degrees(euler[0]))) + "             ")
   screen.addstr(18,0,"Pitch: " + str('{0:.1f}'.format(degrees(euler[1]))) + "             ")
   screen.addstr(19,0,"Yaw:   " + str('{0:.1f}'.format(yaw)) + "          ")
   screen.addstr(20,0,"Yaw_mag:   " + str('{0:.1f}'.format(yaw_mag)) + "          ")
   screen.addstr(22,0,"Distance from Target: " + str('{0:.2f}'.format(distance_from_target)) + " m        ")
   screen.addstr(23,0,"Angle from Target Point: " + str('{0:.1f}'.format(angle_from_target_point)) + " deg     ")
   screen.addstr(24,0,"Angle from Target Orientation: " + str('{0:.1f}'.format(angle_from_target_orientation)) + " deg     ")

def update_geopose(msg):
   global current_geopose
   current_geopose = msg
   screen.addstr(14,0,"Longitude: " + str(current_geopose.position.longitude) + "      ")
   screen.addstr(15,0,"Latitude:  " + str(current_geopose.position.latitude) + "      ")
   screen.addstr(16,0,"Altitude:  " + str(current_geopose.position.altitude) + "      ")

def update_velocity(msg):
   global current_velocity
   current_velocity = msg
   screen.addstr(8,0,"Linear Vel: " + str(round(current_velocity.linear.x, 2)) + " m/s \tAngular Vel: " + str(round(current_velocity.angular.z, 2)) + " rad/s          ")

def update_target_object(msg):
   global no_target_found_counter
   target_found = False
   max_object = 0
   if len(msg.blocks) > 0:
      for i in range(0, len(msg.blocks)):
         if msg.blocks[i].signature == 1:
            #Searching for the largest object to avoid noise
            if max_object < msg.blocks[i].roi.width * msg.blocks[i].roi.height:
               max_object = msg.blocks[i].roi.width * msg.blocks[i].roi.height
               target_found = True
               screen.addstr(18,30,"Target Object     X:   " + str(msg.blocks[i].roi.x_offset - 320 / 2 ) + "       Y: " + str(msg.blocks[i].roi.x_offset - 200 / 2 ) + "           ")
               screen.addstr(19,30,"Target Object Width:   " + str(msg.blocks[i].roi.width) + "  Height: " + str(msg.blocks[i].roi.height) + "           ")
      no_target_found_counter = 0
   else:
      #To avoid noise (one null message at random time)
      no_target_found_counter += 1
   if target_found == False and no_target_found_counter > 1:
      screen.addstr(18,30,"Target Object     X:   0.0       Y: 0.0           ")
      screen.addstr(19,30,"Target Object Width:   0.0  Height: 0.0           ")

def update_state(msg):
   global state
   state = msg.state
   if state == RobotStates.MANUAL_MOVE:
      screen.addstr(25,0, "State: " + str(state) + " Manual Move               ")
   elif state == RobotStates.FOLLOW_OBJECT:
      screen.addstr(25,0,"State: " + str(state) + " Following Object             ")
   elif state == RobotStates.MOVE_TO_POINT:
      screen.addstr(25,0,"State: " + str(state) + " Moving to Point             ")
   else:
      screen.addstr(25,0,"State: " + str(state) + " Automatic Move             ")

def update_target(msg):
   global saved_pose, target_orientation
   saved_pose = msg.pose
   euler = tf.transformations.euler_from_quaternion((msg.pose.orientation.x, msg.pose.orientation.y, msg.pose.orientation.z, msg.pose.orientation.w))
   target_orientation = degrees(euler[2])
   update_distance_from_target()
   update_angle_from_target()
   screen.addstr(14,30,"Longitude: " + str(saved_pose.position.x) + "            ")
   screen.addstr(15,30,"Latitude:  " + str(saved_pose.position.y) + "            ")
   screen.addstr(16,30,"Altitude:  " + str(saved_pose.position.z) + "            ")

def angleFromTarget(currentAngle, targetAngle):
   angle = (targetAngle - currentAngle)
   if angle >= 180:
      angle = angle - 360
   elif angle <= -180:
      angle = angle + 360
   return angle

def update_distance_from_target():
   global distance_from_target
   distance_from_target = distanceBetweenPoints(current_pose, saved_pose)

def update_angle_from_target():
   global angle_from_target_point, angle_from_target_orientation
   angle_from_target_point = angleFromTarget(yaw, angleBetweenPoints(current_pose, saved_pose))
   angle_from_target_orientation = angleFromTarget(yaw, target_orientation)

def update_gps_vel(msg):
   global gps_velocity
   gps_velocity = round(sqrt(pow(msg.twist.linear.x,2) + pow(msg.twist.linear.y , 2)),2)
   screen.addstr(22,45,"GPS Velocity: " + str(gps_velocity) + " m/s        ")

def publish_velocity(pub, linear_velocity, angular_velocity):
   velocity = Twist()
   velocity.linear.x = linear_velocity * speed_percent
   velocity.linear.y = 0
   velocity.linear.z = 0
   velocity.angular.x = 0
   velocity.angular.y = 0
   velocity.angular.z = angular_velocity * speed_percent + MOVE_ANGULAR_VELOCITY * angular_correction_percent
   #velocity.angular.z = (angular_velocity + MOVE_ANGULAR_VELOCITY * angular_correction_percent) * speed_percent
   pub.publish(velocity)

def talker():
   global posestamped_list, pose_list, geopose_list, speed_percent, angular_correction_percent
   rospy.init_node('keyboard_input', anonymous=True)
   pub = rospy.Publisher('cmd_vel', Twist, queue_size=2)
   pub2 = rospy.Publisher("target_pose", PoseStamped, queue_size=2)
   pub3 = rospy.Publisher("robot_state", RobotStates, queue_size=2)
   pub4 = rospy.Publisher("path", Path, queue_size=2)
   pub5 = rospy.Publisher("pose_array", PoseArray, queue_size=2)
   pub6 = rospy.Publisher("syscommand", String, queue_size=2)
   rospy.Subscriber("pose", PoseStamped, update_pose)
   rospy.Subscriber("imu_mag", Imu, update_imu_mag)
   rospy.Subscriber("geopose", GeoPose, update_geopose)
   rospy.Subscriber("battery_state", BatteryState, update_battery)
   rospy.Subscriber("solar_cell_state", BatteryState, update_solar)
   rospy.Subscriber("cmd_vel", Twist, update_velocity)
   rospy.Subscriber("robot_state", RobotStates, update_state)
   rospy.Subscriber("target_pose", PoseStamped, update_target)
   rospy.Subscriber("vel", TwistStamped, update_gps_vel)
   rospy.Subscriber("/my_pixy/block_data", PixyData, update_target_object)
   rate = rospy.Rate(5) # 5hz
   target = PoseStamped()
   savedpath = Path()
   rob_state = RobotStates()
   savedposes = PoseArray()
   hectorreset = String()
   hectorreset.data = "reset"

   linear_velocity = 0
   angular_velocity = 0


   while not rospy.is_shutdown():
      exit = 0
      key = screen.getch()
      if key != curses.ERR:    # This is true if the user pressed something
         #msg.header.stamp = rospy.Time.now() # Note you need to call rospy.init_node() before this will work
         if (key == ord("q")) or (key == ord("Q")) or (key == 27):
            exit = 1
            linear_velocity = 0
            angular_velocity = 0
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == 258) or (key == ord("m")) or (key == ord("M")) or (key == ord("2")):
            screen.addstr(10,0,"Backward")
            linear_velocity = - MOVE_LINEAR_VELOCITY
            angular_velocity = 0
            angular_correction_percent = 0.0
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == 259) or (key == ord("u")) or (key == ord("U")) or (key == ord("8")):
            screen.addstr(10,0,"Forward ")
            linear_velocity = MOVE_LINEAR_VELOCITY
            angular_velocity = 0
            angular_correction_percent = 0.0
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == 260) or (key == ord("h")) or (key == ord("H")) or (key == ord("4")):
            screen.addstr(10,0,"Left    ")
            linear_velocity = 0
            angular_velocity = MOVE_ANGULAR_VELOCITY
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == 261) or (key == ord("k")) or (key == ord("K")) or (key == ord("6")):
            screen.addstr(10,0,"Right   ")
            linear_velocity = 0
            angular_velocity = - MOVE_ANGULAR_VELOCITY
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord("j")) or (key == ord("J") or (key == ord("5"))):
            screen.addstr(10,0,"Stop    ")
            linear_velocity = 0
            angular_velocity = 0
            angular_correction_percent = 0.0
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord("y")) or (key == ord("Y")) or (key == ord("+")) or (key == ord("7")):
            if speed_percent < 1:
               speed_percent += SPEED_PERCENT_STEP
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord("i")) or (key == ord("I")) or (key == ord("-")) or (key == ord("9")):
            if speed_percent > 0:
               speed_percent -= SPEED_PERCENT_STEP
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord("r")) or (key == ord("R")):
            pub6.publish(hectorreset)
         elif (key == ord("n")) or (key == ord("N")) or (key == ord("1")):
            if angular_correction_percent < 1:
               angular_correction_percent += ANGULAR_CORRECTION_STEP
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord(",")) or (key == ord("<")) or (key == ord("3")):
            if angular_correction_percent > -1:
               angular_correction_percent -= ANGULAR_CORRECTION_STEP
            publish_velocity(pub, linear_velocity, angular_velocity)
         elif (key == ord("s")) or (key == ord("S")):
            pose_x = Pose()
            pose_x.orientation = current_pose.orientation
            pose_x.position = current_pose.position
            target.pose = pose_x
            target.header.frame_id = "earth"
            posestamped_list.append(target)
            pose_list.append(pose_x)
            geopose_list.append(current_geopose)
         elif (key == ord("a")) or (key == ord("A")):
            if len(posestamped_list) == 1:
               target.pose = posestamped_list[0].pose
               target.header.frame_id = "earth"
               target.header.stamp = rospy.Time.now()
               pub2.publish(target)
            elif len(posestamped_list) >1:
               savedpath.poses = posestamped_list
               savedpath.header.frame_id = "earth"
               savedpath.header.stamp = rospy.Time.now()
               pub4.publish(savedpath)
               savedposes.poses = pose_list
               savedposes.header.frame_id = "earth"
               savedposes.header.stamp = rospy.Time.now()
               pub5.publish(savedposes)
         elif (key == ord("d")) or (key == ord("D")):
            posestamped_list = []
            pose_list = []
            geopose_list = []
         elif (key == ord("f")) or (key == ord("F")):
            #Read points from file
            posestamped_list = []
            pose_list = []
            geopose_list = []
            with open("/home/ubuntu/catkin_ws/src/robot_core/scripts/POI.txt", "r") as f:
               for line in f.readlines():
                  target = PoseStamped()
                  li = line.strip()
                  if not li.startswith("#"):
                     output = li.split(",")
                     gpoint = GeoPoint()
                     gpoint.latitude = float(output[0])
                     gpoint.longitude = float(output[1])
                     gpoint.altitude = 0.0
                     utmpoint = fromMsg(gpoint)
                     target.pose.position.x = utmpoint.easting
                     target.pose.position.y = utmpoint.northing
                     target.pose.position.z = utmpoint.altitude
                     target.header.frame_id = "earth"
                     quaternion = tf.transformations.quaternion_from_euler(0,0,math.radians(float(output[2])))
                     target.pose.orientation.x = quaternion[0]
                     target.pose.orientation.y = quaternion[1]
                     target.pose.orientation.z = quaternion[2]
                     target.pose.orientation.w = quaternion[3]
                     posestamped_list.append(target)
                     pose_list.append(target.pose)
                     geopose_list.append(gpoint)
         elif (key == ord("g")) or (key == ord("G")):
            #Save points to file
            with open("/home/ubuntu/catkin_ws/src/robot_core/scripts/POI.txt", "w") as f:
               f.write("#latitude,longitude,yaw,frequency\n")
               for temppose in geopose_list:
                  quaternion = (
                     temppose.orientation.x,
                     temppose.orientation.y,
                     temppose.orientation.z,
                     temppose.orientation.w)
                  euler = tf.transformations.euler_from_quaternion(quaternion)
                  f.write(str(temppose.position.latitude) + "," + str(temppose.position.longitude) + "," + str(math.degrees(euler[2])) + "\n")
               f.close()
         elif (key == ord("x")) or (key == ord("X")):
            #Move to last pose
            rob_state.header.stamp = rospy.Time.now()
            rob_state.text = "Follow Object"
            rob_state.state = rob_state.FOLLOW_OBJECT
            pub3.publish(rob_state)
         elif (key == ord("c")) or (key == ord("C")):
            rob_state.header.stamp = rospy.Time.now()
            rob_state.state = 1
            rob_state.text = "Automatic Move"
            pub3.publish(rob_state)            
         elif (key == ord("z")) or (key == ord("Z")):
            rob_state.header.stamp = rospy.Time.now()
            rob_state.state = rob_state.MANUAL_MOVE
            rob_state.text = "Manual Move"
            pub3.publish(rob_state)         

         screen.addstr(9,0,"Speed %: " + str(round(speed_percent * 100, 0)) + " \t\tAngular %: " + str(round(angular_correction_percent * 100, 0)) + "         ")
         screen.addstr(14,60, "Poses Saved: " + str(len(posestamped_list)) + "     ")
         #rospy.loginfo(msg)

         if exit == 1:
            curses.endwin()
            rospy.signal_shutdown("Terminated by user")
            rospy.sleep(1)
            break

      rate.sleep()
   curses.endwin()


if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
