//BBB_PWM PINS
#define LEFT_MOTOR_PIN P8_13
#define RIGHT_MOTOR_PIN P9_16

/*BBB_UART PINS 
//P9_11 UART04_RX
//P9_13 UART04_TX
//P9_22 UART02_RX
//P9_21 UART02_TX
//IMU ON UART_04
//GPS ON UART_02 */

//UNCOMMENT TO ENABLE
#define GPS_ENABLE
#define IMU_ENABLE
#define PWM_ENABLE
#define IMU_PORT "/dev/ttyO4"
#define GPS_PORT "/dev/ttyO2"
//#define IMU_PORT "/dev/ttyUSB0"
//#define GPS_PORT "/dev/ttyUSB0"

//ERROR TIMERS TIMEOUT
#define SERIAL_TIMEOUT 5 //seconds to reach serial timeout
#define TIME_WITH_NO_ANGULAR_MOVE 20 //seconds to reach angle timeout
#define TIME_WITH_NO_LINEAR_MOVE 40 //seconds to reach distance timemout
#define IMU_REFRESH_TIME 200000 //microseconds
#define GPS_REFRESH_TIME 1000000 //microseconds
#define GPS_LOG_REFRESH_TIME 5000000 //microseconds

//OTHER CONFIGURATION (FOR BLACKLIB PWM FUNCTION)
#define NEUTRAL_FREQUENCY 7.5
#define MAX_FREQUENCY 1.5  //distance from NEUTRAL_FREQUENCY
#define PERIOD_TIME 20000000 //nanonsecond


//DEFINITIONS FOR PROGRAMM
//DO NOT CHANGE
#define LEFT_MOTOR 1
#define RIGHT_MOTOR 2
#define FORWARD 1
#define STOP 0
#define REVERSE -1
