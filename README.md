# README #

This is a repository of the Ros workspace for reDevil (atrv-mini)

Installation Guide on BeagleBone Black

# **BeagleBone Black Settings** #

## 1. Network settings ##

```
#!linux

sudo nano /etc/network/interfaces
```

add the following: 

```
#!

#My Primary network interface
auto eth0
   iface eth0 inet static
   address 147.27.9.53
   netmask 255.255.255.0
   network 147.27.9.0
   dns-nameservers 147.27.13.1
   gateway 147.27.9.254
```

```
#!linux


sudo reboot
```


## 2. Add user ##
[https://www.digitalocean.com/community/tutorials/how-to-add-delete-and-grant-sudo-privileges-to-users-on-a-debian-vps](https://www.digitalocean.com/community/tutorials/how-to-add-delete-and-grant-sudo-privileges-to-users-on-a-debian-vps)

Login as root:

```
#!linux

visudo
# User privilege specification
root        ALL=(ALL:ALL) ALL
newuser    ALL=(ALL:ALL) ALL
save
sudo reboot  
```


## 3. WIFI AD-HOC settings ##
sudo nano /etc/network/interfaces
add the following:

```
#!linux

#My Wifi
auto wlan0
   iface wlan0 inet static
   address 192.168.1.2
   netmask 255.255.255.0
   gateway 192.168.1.1
   wireless-mode ad-hoc
   wireless-essid BBB-reDevil
```


```
#!linux

sudo reboot 
```
      
   
## 4. Enable GPIO pin ##

```
#!linux

Login as root:
cd /sys/class/gpio
echo pin > export 
# pin = is the pin that I want to enable  
sudo reboot
```


## 5. Enable UART (Serial) ports and PWM (kernel 3.8) ##
To enable serial ports in debian 7.9
	sudo nano /etc/default/capemgr
in the last line write
	CAPE=BB-UART2,BB-UART4,BB-PWM2
add other ports separated by comma
more info 
[http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes](http://elinux.org/Beagleboard:BeagleBoneBlack_Debian#Loading_custom_capes)
UART PINS
[http://beagleboard.org/Support/bone101/#headers](http://beagleboard.org/Support/bone101/#headers)


## 6. Enable UART and PWM pins (kernel 4.x) ##

Check your Linux kernel


```
#!linux

uname -a
```
if you run 4.x kernel follow the following instructions to install update dtc and updated overlays [https://github.com/beagleboard/bb.org-overlays
](https://github.com/beagleboard/bb.org-overlays)


update kernel to latest:

```
#!linux

cd /opt/scripts/tools
git pull
sudo ./update_kernel.sh --lts-4_4 --ti-channel
```



add the following line to /boot/uEnv.txt


```
#!linux

cape_universal=enable
```

you can also disable HDMI (Audio/Video) in /boot/uEnv.txt


add the follwing line to /etc/default/capemgr


```
#!linux

CAPE=cape-universala
```

to enable UART pins download config-pin from [https://github.com/cdsteinkuehler/beaglebone-universal-io](https://github.com/cdsteinkuehler/beaglebone-universal-io)
and run


```
#!linux

sudo ./config-pin P9_21 uart #UART2
sudo ./config-pin P9_22 uart #UART2
sudo ./config-pin P9_11 uart #UART4
sudo ./config-pin P9_13 uart #UART4
```

to enable eQEP pins run


```
#!linux

./config-pin P8_11 qep #eQEP2B
./config-pin P8_12 qep #eQEP2A
./config-pin P9_27 qep #eQEP0B
./config-pin P9_92 qep #eQEP0A
```


or to enable at reboot add to /etc/rc.local the following before exit 0


```
#!linux
./home/ubuntu/catkin_ws/config-pin P9_21 uart #UART2
./home/ubuntu/catkin_ws/config-pin P9_22 uart #UART2
./home/ubuntu/catkin_ws/config-pin P9_11 uart #UART4
./home/ubuntu/catkin_ws/config-pin P9_13 uart #UART4
./home/ubuntu/catkin_ws/config-pin P8_11 qep #eQEP2B
./home/ubuntu/catkin_ws/config-pin P8_12 qep #eQEP2A
./home/ubuntu/catkin_ws/config-pin P9_27 qep #eQEP0B
./home/ubuntu/catkin_ws/config-pin P9_92 qep #eQEP0A

```



Read [https://github.com/adafruit/adafruit-beaglebone-io-python/issues/80
](https://github.com/adafruit/adafruit-beaglebone-io-python/issues/80
)

Install latest adafruit-beaglebone-io-python following the instructions

[https://github.com/adafruit/adafruit-beaglebone-io-python](https://github.com/adafruit/adafruit-beaglebone-io-python)
[https://learn.adafruit.com/setting-up-io-python-library-on-beaglebone-black/pwm](https://learn.adafruit.com/setting-up-io-python-library-on-beaglebone-black/pwm)



# **BeagleBone Black Ros Install** #

## 1. Download Ubuntu ARM image ##
[https://rcn-ee.com/rootfs/2016-06-09/flasher/BBB-eMMC-flasher-ubuntu-16.04-console-armhf-2016-06-09-2gb.img.xz](https://rcn-ee.com/rootfs/2016-06-09/flasher/BBB-eMMC-flasher-ubuntu-16.04-console-armhf-2016-06-09-2gb.img.xz)

## 2. Copy image to microSD card ##
windows: 
[https://wiki.ubuntu.com/Win32DiskImager](https://wiki.ubuntu.com/Win32DiskImager)

linux: 

```
#!linux

xzcat BBB-eMMC-flasher-ubuntu-16.04-console-armhf-2016-06-09-2gb.img.xz | sudo dd of=/dev/sdX
```


## 3. Fix Timezone for Greece ##

```
#!linux

	date (check the date-time)
	sudo rm /etc/locatime
	sudo ln -s /usr/share/zoneinfo/Europe/Athens /etc/localtime
	date (see if it is fixed)
```


## 4. Follow Ubuntu ROS installation instrunctions ## 
In [http://wiki.ros.org/indigo/Installation/UbuntuARM](http://wiki.ros.org/indigo/Installation/UbuntuARM)

Setup locales

Setup sources.list http://wiki.ros.org/kinetic/Installation/Ubuntu


```
#!linux

	sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
	sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 0xB01FA116
	sudo apt-get update
```


Install base ROS

```
#!linux

	sudo apt-get install ros-kinetic-ros-base
    sudo rosdep init
    rosdep update
    
    sudo apt-get install python-rosinstall python-rosinstall-generator python-wstool build-essential
    
    echo "source /opt/ros/kinetic/setup.bash" >> ~/.bashrc
    source ~/.bashrc
```

Do not forget to run


```
#!linux

	echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc
```


in case roslint package not found run:

```
#!ROS

rosdep install --from-paths src --ignore-src --rosdistro indigo -y
```

## 5 Running ROS in multiple machines ##
run

```
#!linux

export ROS_IP=XXX.XXX.XXX.XXX
export ROS_MASTER_URI=http://XXX.XXX.XXX.XXX:11311
```


to print variables write:

```
#!linux

echo $ROS_IP
echo $ROS_MASTER_URI
```